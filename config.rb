# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "themes/groupementdegestion/css/"
sass_dir = "themes/groupementdegestion/_sass/"
images_dir = "themes/groupementdegestion/img/"
javascripts_dir = "themes/groupementdegestion/js/"

output_style = :compact
environment = :production

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false
color_output = false

# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass Users/Thomas/Sites/centre-lotus/cp/expressionengine/templates/default_site/_sass scss && rm -rf sass && mv scss sass

line_comments = false # by Fire.app 
output_style = :compressed # by Fire.app 