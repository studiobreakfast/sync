<?php

$lang = array(

//----------------------------
// Publish page
//----------------------------

"layout_success" => 
"Disposition enregistrée avec succès.",

"layout_failure" => 
"Un problème a été rencontré lors de la sauvegarde de votre disposition.",

"url_title_is_index" =>
"Le titre URL ne peut être intitulé \"index\"",

"no_related_entries" =>
"Aucun article n'existe pour ce canal relié",

"localized_date" =>
"Localisé",

"fixed_date" =>
"Fixé",

"calendar" =>
"Calendrier",

"edit_categories" =>
"Editer les catégories",

"revisions" =>
"Révisions",

"rev_date" =>
"Date",

"rev_author" =>
"Auteur",

"revision" =>
"Révision",

"versioning_enabled" =>
"Activer les révision pour cet article",

"revision_warning" =>
"Vous êtes sur le point de charger une version précédente. Tout le contenu non sauvegardé actuellement sur cette page sera perdu.",

"load_revision" =>
"Charger une révision",

"current_rev" =>
"Actuellement chargée",

"version_preview" =>
"Révision numéro %s",

"no_revisions_exist" =>
"Il n'y a actuellement aucune révision pour cet article.",

"dst_enabled" =>
"Heure d'été active pour la date de l'article",

"multi_entries_updated" =>
"Les articles ont été mis à jour",

"multi_entry_editor" =>
"Editeur multi-articles",

"unauthorized_to_edit" =>
"Vous n'êtes pas autorisé à éditer les articles sélectionnés.",

"today" =>
"Aujourd'hui",

"edit_selected" =>
"Editer la sélection",

"delete_selected" =>
"Supprimer la sélection",

"forum" =>
"Forum",

"forum_topic_id" =>
"ID du sujet de forum",

"forum_topic_id_info" =>
"Ce champ permet à cet article d'être associé avec un sujet de forum.",

"forum_topic_id_exitsts" =>
"Si un sujet de forum existe déjà et que vous souhaitez l'associer avec votre article, saisissez le numéro d'ID de sujet et laissez les champs ci-dessus vides.",

"forum_title" =>
"Titre du sujet de forum",

"forum_body" =>
"Texte du sujet de forum",

"forums_unavailable" =>
"Il n'y a aucun forum de disponible dans lequel publier",

'view_comments' =>
"Voir les commentaires",

'live_look' =>
'Aperçu rapide',

"delete_comments_confirm" =>
"Etes-vous sûr de vouloir définitivement supprimer ces commentaires ?",

'field_blank' =>
"Vous avez laissé un champ vide.",

"select_action" =>
"Sélectionnez vos options de placement",

"image_location" =>
"Emplacement de l'image",

"file_uploaded" =>
"Fichier téléchargé :",

"place_file" =>
"Placer le fichier",

"place_image" =>
"Placer l'image",

"place_file_close" =>
"Placer le fichier et fermer la fenêtre",

"place_image_close" =>
"Placer l'image et fermer la fenêtre",

"embedded" =>
"Inclure dans l'article",

"popup_link" =>
"Lien pop-up de l'image",

"popup_thumb" =>
"Lien pop-up de la miniature",

"thumb_info" =>
"Si vous ne souhaitez pas redimensionner ou créer une vignette, passez les options ci-dessous",

"check_spelling" =>
"Vérifier l'orthographe",

"spell_check" =>
"Vérificateur d'orthographe",

"no_spellcheck_lib" =>
"Erreur : la bibliothèque Pspell ne semble pas installée.",

"html_glossary" =>
"Glossaire",

"no_glossary" =>
"Aucun glossaire n'existe",

"no_smileys" =>
"Impossible de localiser les émoticônes",

"image_link" =>
"Lien de l'image",

"font" =>
"Police",

"heading_1" =>
"Titrage 1",

"heading_2" =>
"Titrage 2",

"heading_3" =>
"Titrage 3",

"heading_4" =>
"Titrage 4",

"heading_5" =>
"Titrage 5",

"paragraph" =>
"Paragraphe",

"blockquote" =>
"Citation",

"pre" =>
"Pre-formaté",

"div" =>
"Div",

"span" =>
"Span",

"anchor" =>
"Hyperlien",

"clear" =>
"Effacer",

"bold" =>
"Gras",

"italic" =>
"Italique",

"unordered_list" =>
"Liste désordonnée",

"ordered_list" =>
"Liste ordonnée",

"horizontal_rule" =>
"Règle horizontale",

"nonbr_space" =>
"Espace insécable",

"line_break" =>
"Retour à la ligne",

"publish_form" =>
"Formulaire de publication",

"pings" =>
"Pings",

"no_ping_sites" =>
"Aucun serveur de Ping n'existe actuellement.",

"entry_date" => 
"Date de l'article",

"url_title_is_numeric" =>
"Les nombres ne peuvent être utilisés comme titres URL",

'unable_to_create_url_title' =>
'Impossible de créer un titre URL valide pour votre article',

"select_all" =>
"Sélectionner/Désélectionner tout",

"results" =>
"résultats",

"close_for_no_change" =>
"Fermer cette fenêtre pour annuler.",

"date_calendar" =>
"Calendrier Date",

"comment_expiration_date" =>
"Date d'expiration de commentaire",

"invalid_comment_date_formatting" => 
"La date d'expiration de commentaire que vous avez saisie n'était pas formatée correctement (année-mois-jour heure:minute). Laissez le champ vide si vous ne souhaitez pas que vos commentaires expirent.",

"keywords" =>
"Mots clés",

"title_only" =>
"Rechercher dans les titres uniquement",

"title_and_body" =>
"Rechercher dans les titres et les articles",

"title_body_comments" =>
"Rechercher dans les titres, les articles et les commentaires",

"exact_match" =>
"Correspondance exacte",

"search" =>
"Rechercher",

"filter_by_channel" =>
"Filtrer par canal",

"filter_by_category" =>
"Filtrer par catégorie",

"filter_by_status" =>
"Filtrer par statut",

"filter_by_author" =>
"Filtrer par auteur",

"status_changed" =>
"Statut modifié",

"publish" => 
"Publier",

"no_channel_exists" => 
"Vous avez tenté d'accéder à un canal qui n'existe pas.",

"no_entry_to_update" =>
"Vous avez tenté de mettre à jour un article qui n'existe pas.",

"no_entries_matching_that_criteria" => 
"Aucun article ne correspond aux critères que vous avez saisis",

"no_entries_exist" => 
"Il n'y a aucun article dans ce canal",

"select_channel_to_post_in" => 
"Choisissez un canal dans lequel publier",

"select_channel_to_edit" => 
"Choisissez un canal à consulter",

"this_entry_will_appear_in" => 
"Cet article apparaîtra dans :",

"unauthorized_for_this_channel" => 
"Vous n'êtes pas autorisé à publier dans ce canal",

"unauthorized_for_any_channels" => 
"Vous n'êtes pas autorisé à publier des articles",

"unauthorized_to_delete_others" =>
"Vous êtes uniquement autorisé à supprimer des articles que vous avez rédigés",

"unauthorized_to_delete_self" =>
"Vous n'êtes pas autorisé à supprimer vos propres articles",

"channel" =>
"Canal",

"channels" =>
"Canaux",

"new_entry" =>
"Nouvel article",

"button_mode" =>
"Mode bouton :",

"guided" =>
"Guidé",

"normal" =>
"Normal",

"view_entry" =>
"Voir l'article",

"preview_entry" =>
"Prévisualiser l'article",

"edit_entry" =>
"Editer l'article",

"edit_channel_entries" =>
"Editer les articles",

"continue_editing" => 
"Continuer l'édition",

"entry_status" => 
"Statut de l'article",

"status" => 
"Statut",

"comments" => 
"Commentaires",

"open" => 
"Ouvert",

"close" => 
"Fermer",

"closed" => 
"Fermé",

"options" => 
"Options",

"sticky" => 
"Faire remonter l'article",

"allow_comments" => 
"Autoriser les commentaires",

"ping_sites" => 
"Sites pour le Ping",

"select_entries_to_ping" =>
"Sélectionnez les articles pour lesquels effectuer un Ping",

"view_previous_pings" => 
"Voir les Pings précédents",

"previously_pinged_urls" => 
"Vous avez effectué précédemment des Pings pour les URLs suivantes",

"date" => 
"Date",

"start_date" =>
'Début',

"end_date" =>
'Fin',

"expiration_date" => 
"Date d'expiration",

"invalid_date_formatting" => 
"La date que vous avez saisie n'est pas formatée correctement (année-mois-jour heure:minute)",

"invalid_date" =>
"La date que vous avez saisie n'est pas valide.",

"date_outside_of_range" => 
"La date que vous avez saisie est en dehors de la plage acceptée (1902 - 2037).",

"category" =>
"Catégorie",

"categories" => 
"Catégories",

"no_categories" => 
"Aucune catégorie n'a été assignée à ce canal.",

"title" => 
"Titre",

"url_title" => 
"Titre URL",

"author" => 
"Auteur",

"newline_format" => 
"Formatage :",

"none" => 
"Aucun",

"link" => 
"Lien",

"image" => 
"Image",

"email" => 
"Email",

"upload" => 
"Télécharger",

'upload_file' =>
'Télécharger fichier',

"file_upload" =>
"Téléchargement de fichier",

'upload_dir_choose'		=>
'Choisissez un répertoire de téléchargement',

"emoticons" =>
"Emoticônes",

"choose_a_destination_for_emoticon" =>
"Choisissez une destination pour vos émoticônes",

"click_emoticon" =>
"Cliquez sur une image pour l'insérer dans votre article",

"no_emoticons" =>
"Impossible de trouver la localisation de vos émoticônes",

"close_all" => 
"Fermer tout",

"missing_title" =>
"Vos articles doivent avoir un titre.",

"missing_date" =>
"Vos articles doivent avoir une date.",

"title_not_unique" =>
"Un article portant ce nom existe déjà. Les titres doivent être unique.",

"url_title_not_unique" =>
"Un article avec cette URL existe déjà. Les URLs doivent être unique.",

"entry_has_been_added" =>
"Nouvel article soumis",

"entry_has_been_updated" =>
"L'article a été mis à jour",

"xmlrpc_ping_errors" =>
"Les Pings XML-RPC suivants n'ont pas été acceptés :",

"click_to_view_your_entry" =>
"Cliquez ici pour voir votre article",

"view" => 
"Voir",

"edit_this_entry" => 
"Editer cet article",

"date_range" =>
"Intervalle de date",

"today" =>
"Aujourd'hui",

"past_week" =>
"Les 7 derniers jours",

"past_month" =>
"Les 30 derniers jours",

"past_six_months" =>
"Les 180 derniers jours",

"past_year" =>
"Les 365 derniers jours",

"any_date" =>
"N'importe quelle date",

"results_per_page" =>
"Résultats par page",

"order" =>
"Ordre",

"ascending" =>
"Le plus ancien en premier",

"descending" =>
"Le plus récent en premier",

"alpha" =>
"Alphabétique",

"delete_confirm" =>
"Confirmation de suppression",

"delete_entry_confirm" =>
"Etes-vous sûr de vouloir définitivement supprimer cet article ?",

"delete_entries_confirm" =>
"Etes-vous sûr de vouloir définitivement supprimer ces articles ?",

"entries_deleted" =>
"Supprimé",

"html_buttons_no_cursor" =>
"Pour pouvoir utiliser ces boutons vous devez tout d'abord placer votre curseur dans un champ.",

"html_buttons_url_text" =>
"Entrez l'URL hypertexte",

"html_buttons_webpage_text" =>
"Entrez le titre du lien",

"html_buttons_title_text" =>
"Optionnel : Entrez un attribut de titre",

"html_buttons_image_text" =>
"Entrez l'URL de l'image",

"html_buttons_email_text" =>
"Entrez l'adresse email",

"html_buttons_email_title" =>
"Entrez le titre du lien (ou laissez le champ vide pour utiliser l'adresse email comme titre.)",

"html_buttons_enter_text" =>
"Entrez le texte que vous souhaitez formater",

"warning" =>
"Avertissement",

"file_exists" =>
"Un fichier portant ce nom existe déjà",

"overwrite_instructions" =>
"Vous pouvez soit choisir un nouveau nom et le fichier sera renommé, soit soumettre ce formulaire avec le même nom et le fichier sera écrasé.",

"select_destination_dir" =>
"Sélectionner un répertoire de destination",

"file_type" =>
"Type de fichier",

"image" =>
"Image",

"non_image" =>
"Autre qu'image",

"success" =>
"Succès !",

"you_have_uploaded" =>
"Vous avez téléchargé le fichier suivant :",

"choose_a_destination" =>
"Où souhaitez-vous que votre fichier apparaisse ?",

"no_entry_fields" =>
"Il n'y a aucun champ d'article dans ce canal.",

"fields" =>
"Champs",

"no_comments" =>
"Aucun résultat retourné pour cet article",

"posted_by" =>
"Auteur :",

"located_in" =>
"Emplacement :",

"comment_date" =>
"Date :",

"comment_email" =>
"Email :",

"comment_url" =>
"URL :",

"comment_action" =>
"Action :",

"comment_ip" =>
"IP :",

"channel_name" =>
"Canal :",

"comment" =>
"Commentaire",

"missing_comment" =>
"Vous devez saisir un commentaire",

"edit_comment" =>
"Editer le commentaire",

"delete_comment" =>
"Supprimer les commentaires",

"comment_deleted" =>
"Commentaires supprimés",

"comment_updated" =>
"Commentaire mis à jour",

"delete_comment_confirm" =>
"Etes-vous sûr de vouloir définitivement supprimer ce commentaire ?",

"name" =>
"Nom",

"url" =>
"URL",

"location" =>
"Emplacement",

"continue" =>
"Continuer",

"image_options" =>
"Options d'image",

"create_popup" =>
"Créer un pop-up pour l'image ?",

"create_thumb" =>
"Créer une miniature pour l'image ?",

"constrain_proportions" =>
"Conserver les proportions",

"thumb_settings" =>
"Paramètres d'image",

"choose_a_destination_for_thumb" =>
"Où souhaitez-vous que cette image apparaisse ?",

"do_not_place_file" =>
"Ne pas placer le fichier dans l'article",

"image_size_not_different" =>
"La largeur et la hauteur que vous avez saisies sont les mêmes que l'original.",

"width" =>
"Largeur",

"height" =>
"Hauteur",

"pixels" =>
"Pixels",

"percent" =>
"Pourcent",

"resize_image" =>
"Redimensionner l'image",

"create_thumb_copy" =>
"Créer une copie séparée",

"resize_original" =>
"Redimensionner l'image originale",

"thumb_instructions" =>
"Vous pouvez soit redimensionner votre image, soit créer une miniature pour l'image.",

"thumbnail_created" =>
"Miniature créée",

"image_resized" =>
"Votre image a été redimensionnée",

"close_window" =>
"Fermer la fenêtre",

'close_selected' =>
"Fermer la sélection",

'open_selected' =>
"Ouvrir la sélection",

'instructions' =>
"Instructions : ",

'choose_entry_for_comment_move' =>
"Choisissez un article pour le déplacement de commentaire",

'move_comments_to_entry' =>
"Déplacer les commentaires vers l'article",

'move_selected' =>
"Déplacer la sélection",

'choose_only_one_entry' =>
"Vous ne devez choisir qu'un seul article",

'comment_moved' =>
"Commentaire déplacé", 

'comments_moved' =>
"Commentaires déplacés",

'add_categories' =>
"Ajouter des catégories",

'remove_categories' =>
"Enlever des catégories",

'no_category_group_match' =>
"Aucune correspondance de groupe de catégories n'a été trouvé pour tous les articles. Veuillez choisir des articles qui ont au moins un groupe de catégories en commun.",

'multi_entry_category_editor' =>
"Editeur de catégorie multi-article",

'url_only' =>
"URL uniquement",

'no_channels' =>
"Aucun canal ne vous est assigné pour ce site.",

"entry_title_with_title" =>
"Titre de l'article : '%title'",

'invalid_author' =>
"L'auteur sélectionné est invalide.",

'show_all' =>
"Montrer tout",


''=>''
);


/* End of file lang.publish.php */
/* Location: ./system/expressionengine/language/french/lang.publish.php */