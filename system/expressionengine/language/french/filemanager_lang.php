<?php

$lang = array(

'file_title'    	=> 'Titre du fichier',
'dir_name'				=> 'Nom du répertoire',
'file_metadata'			=> 'Données',
'description'			=> 'Description',
'credit'				=> 'Crédits',
'filter_by_directory'	=> 'Filtrer par répertoire',
'search_files'			=> 'Recherche de fichiers',
'search_in'				=> 'Rechercher dans...',
'custom_fields'			=> 'Champs personnalisés',
'file_directory_id'		=> '#',

'wm_name'				=> 'Nom du filigrane',
'wm_type'				=> 'Type de filigrane',
'no_watermarks'			=> 'Aucun filigrane',
'wm_watermark'			=> 'Filigrane',
'add_watermark'			=> 'Ajouter un filigrane',

'setting'				=> 'Paramètre',
'preference'			=> 'Préférence',

'category_groups'		=> 'Groupes de catégorie',
'no_assigned_category_groups' => 'Aucun groupe assigné',
'image_sizes'			=> 'Manipulation des images',
'image_sizes_subtext'	=> 'Les images additionelles seront créées automatiquement à chaque téléchargement',
'short_name'			=> 'Nom court',

'resize_type'			=> 'Type de redimensionnement',
'constrain'				=> 'Étroit',

'content_files' 		=> 'Gestionnaire de fichiers',

'upload_dir_choose'		=> 'Choisissez un répertoire de téléchargement',
'file_upload_prefs' 	=> 'Préférences du téléchargement de fichier',
'create_new_upload_pref' => 'Créer un nouveau répertoire de téléchargement',
'file_information'		=> 'Information du fichier',
'upload' 				=> 'Télécharger',
'upload_file' 			=> 'Envoyer le fichier',
'file_upload' 			=> 'Envoyer un fichier',
'file_download' 		=> 'Envoyer',
'file_tools'			=> 'Outils',
'choose_file'			=> 'Veuillez choisir un fichier à supprimer',
'confirm_del_file'		=> 'Etes-vous sûr de vouloir définitivement supprimer ce fichier ?',
'confirm_del_files'		=> 'Etes-vous sûr de vouloir définitivement supprimer ces fichiers ?',
'delete_success'		=> 'Suppression réussie',
'delete_fail'			=> "Il y a eu un problème lors de la suppression d'un ou plusieurs fichier(s). Veuillez vérifier la liste ci-dessous.",
'no_file'				=> 'Aucun fichier sélectionné',
'file_name'				=> 'Nom du fichier',
'file_size'				=> 'Taille du fichier',
'file_size_unit'		=> 'KB',
'size'					=> 'Taille',
'kind'					=> 'Type',
'where'					=> 'Où',
'permissions'			=> 'Permissions',
'upload_success'		=> 'Téléchargement réussi',
'no_upload_dirs'		=> "Vous n'avez aucun répertoire de téléchargement de défini",
'no_uploaded_files'		=> "Vous n'avez téléchargé aucun fichier dans ce répertoire",
'image_editor'			=> "Editeur d'images",
'download_selected'		=> "Télécharger les fichiers sélectionnés",
'email_files'			=> 'Envoyer les fichiers sélectionnés par email',
'delete_selected_files'	=> 'Supprimer les fichiers sélectionnés',

'edit_modes'			=> "Modes d'édition",
'resize'				=> 'Redimensionner',
'crop'					=> 'Rogner',
'resize_width'			=> 'Largeur',
'resize_height'			=> 'Hauteur',
'crop_width'			=> 'Largeur',
'crop_height'			=> 'Hauteur',
'crop_x'				=> 'X',
'crop_y'				=> 'Y',
'rotate'				=> 'Rotation',
"rotate_90r"			=> "90&#176; à droite",
"rotate_90l"			=> "90&#176; à gauche",
"rotate_180"			=> "180&#176",
"rotate_flip_vert"		=> "Basculer verticalement",
"rotate_flip_hor"		=> "Basculer horizontalement",
"maintain_ratio"		=> "Conserver les proportions",
'width_needed'			=> "Une largeur, une largeur/hauteur, ou une direction de rotation doit être sélectionnée",

"crop_mode"				=> "Mode Rognage",
"resize_mode"			=> "Mode Redimensionnement",
"rotate_mode"			=> "Mode Rotation",
'apply_changes'			=> 'Appliquer les modifications ?',
'exit_apply_changes'	=> "Vous êtes sur le point de quitter le mode édition. Souhaitez-vous d'abord appliquer les modifications ?",
'processing_image'		=> "Traitement de l'image",
'done'					=> "Sortir",
'edit_image'			=> "Sauvegarder l'image",
'image_edit_success'	=> "Image éditée avec succès",
'no_edit_selected'		=> "Aucune opération d'édition sélectionnée",


"edit_file_upload_preferences" =>
"Editer les préférences de téléchargement de fichiers",

"new_file_upload_preferences" =>
"Nouvelle préférence de téléchargement de fichiers",

"new_file_upload_created" =>
"Nouvelle destination de téléchargement créée",

"file_upload_preferences" =>
"Préférences du téléchargement de fichiers",

"no_upload_prefs" =>
"Il n'y a actuellement aucune préférence pour le téléchargement de fichiers",

"create_new_upload_pref" =>
"Créer une nouvelle destination de téléchargement",

"upload_pref_name" =>
"Nom du répertoire de téléchargement",

"new_file_upload_preferences" =>
"Nouvelle destination de téléchargement",

"server_path" =>
"Chemin serveur vers le répertoire de téléchargement",

"url_to_upload_dir" =>
"URL du répertoire de téléchargement",

"allowed_types" =>
"Types de fichiers autorisés",

"max_size" =>
"Taille maximale de fichier (en bytes)",

"max_height" =>
"Hauteur maximale d'image (en pixels)",

"max_width" =>
"Largeur maximale d'image",

"properties" =>
"Propriétés de l'image",

"pre_format" =>
"Pré-formatage de l'image",

"post_format" =>
"Post-formatage de l'image",

"no_upload_dir_name" =>
"Vous devez saisir un nom pour votre répertoire de téléchargement",

"no_upload_dir_path" =>
"Vous devez saisir le chemin vers répertoire de téléchargement",

"no_upload_dir_url" =>
"Vous devez saisir l'URL de votre répertoire de téléchargement",

"duplicate_dir_name" =>
"Le nom de votre répertoire est déjà utilisé",

"delete_upload_preference" =>
"Supprimer les préférence de téléchargement",

"delete_upload_pref_confirmation" =>
"Etes-vous sûr de vouloir définitivement supprimer ces préférences ?",

"upload_pref_deleted" =>
"Préférences de téléchargement supprimées :",

"current_upload_prefs" =>
"Préférences actuelles",

"restrict_to_group" =>
"Restreindre le téléchargement de fichier à certains groupes de membres",

"restrict_notes_1" =>
"Ces boutons radios vous permettent de spécifier quels groupes de membres seront autorisés à télécharger des fichiers.",

"restrict_notes_2" =>
"Les Supers Administrateurs peuvent toujours télécharger des fichiers",

"restrict_notes_3" =>
"Note : le téléchargement de fichier n'est actuellement autorisé que via le tableau de bord",

"can_upload_files" =>
"Peut télécharger des fichiers",

"images_only" =>
"Images uniquement",

"all_filetypes" =>
"Tout type de fichiers",

'file_properties' =>
"Propriété du fichier",

'file_pre_format' =>
"Pré-formatage du fichier",

'file_post_format' =>
"Post-formatage du fichier",


''=>''
);

/* End of file lang.filemanager.php */
/* Location: ./system/expressionengine/language/english/lang.filemanager.php */