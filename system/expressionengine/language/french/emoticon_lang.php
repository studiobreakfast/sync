<?php

$lang = array(

//----------------------------------------
// Required for MODULES page
//----------------------------------------

"emoticon_module_name" =>
"Emoticône",

"emoticon_module_description" =>
"Module d'émoticônes (smileys)",


//----------------------------------------
// Emoticon language lines
//----------------------------------------

"emoticon_heading" =>
"Emoticône",

"emoticon_glyph" =>
"Glyphe",

"emoticon_image" =>
"Image",

"emoticon_width" =>
"Largeur",

"emoticon_height" =>
"Hauteur",

"emoticon_alt" =>
"Balise Alt",



''=>''
);

/* End of file lang.emoticon.php */
/* Location: ./system/expressionengine/language/french/lang.emoticon.php */