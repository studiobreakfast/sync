<?php

$lang = array(

//----------------------------------------
// Required for MODULES page
//----------------------------------------

"comment_module_name" =>
"Commentaires",

"comment_module_description" =>
"Module de commentaires",

//----------------------------------------

"cmt_invalid_form_submission" =>
"Soumission du formulaire invalide.",

"cmt_commenting_has_expired" =>
"Les commentaires ne sont pas disponibles pour cet article.",

"cmt_name_not_allowed" =>
"Le nom avec lequel vous souhaiter publier est réservé. Veuillez choisir un nom différent.",

"cmt_must_be_member" =>
"Vous devez être un membre inscrit pour publier des commentaires",

"cmt_no_authorized_for_comments" =>
"Vous n'êtes pas autorisé à publier des commentaires",

"cmt_account_not_active" =>
"Votre compte de membre n'a pas encore été activé",

"cmt_comments_not_allowed" =>
"Les commentaires ne sont pas autorisés pour ce canal",

"cmt_missing_name" =>
"Le champ nom est requis",

"cmt_missing_comment" =>
"Le champ commentaire est requis",

"cmt_missing_email" =>
"Le champ email est requis",

"cmt_invalid_email" =>
"L'adresse email que vous avez saisie est invalide",

"cmt_banned_email" =>
"L'adresse email que vous avez saisie est exclue",

"cmt_too_large" =>
"Le commentaire que vous avez saisi contient %n caractères. Seuls %x caractères sont autorisés.",

"cmt_comments_timelock" =>
"Vous n'êtes autorisé à publier des commentaires que toutes les %s secondes.",

"cmt_duplicate_comment_warning" =>
"Impossible de recevoir votre commentaire pour le moment.",

"cmt_no_preview_template_specified" =>
"La balise du formulaire de commentaire ne contient pas d'information sur la localisation de votre modèle d'aperçu.",

"cmt_comment_response_title" =>
"Quelqu'un a répondu à votre commentaire",

"cmt_comment_response_body" =>
"Quelqu'un a répondu à l'article auquel vous vous êtes abonné à :",

"cmt_comment_title" =>
"Le titre de l'article est :",

"cmt_comment_url" =>
"Vous pouvez consulter le commentaire à l'adresse suivante :",

"cmt_comment_removal" =>
"Pour ne plus recevoir de notifications pour ce commentaire, cliquez ici :",

"cmt_comment_removal_all" =>
"Pour ne plus recevoir aucune notification, cliquez ici :",

"cmt_notification_removal" =>
"Arrêt des notifications",

"cmt_you_have_been_removed" =>
"Vous êtes désinscrit et ne recevrez plus les futures notifications.",

"cmt_comment_accepted" =>
"Commentaire accepté",

"cmt_will_be_reviewed" =>
"Votre commentaire sera soumis à un modérateur pour validation.",

"cmt_return_to_comments" =>
"Retourner aux commentaires",


''=>''
);

/* End of file lang.comment.php */
/* Location: ./system/expressionengine/language/french/lang.comment.php */