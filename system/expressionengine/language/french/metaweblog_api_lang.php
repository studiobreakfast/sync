<?php

$lang = array(

/* ----------------------------------------
/*  Required for MODULES page
/* ----------------------------------------*/
"metaweblog_api_module_name" =>
"API Metaweblog",

"metaweblog_api_module_description" =>
"Module d'API Metaweblog",

/*----------------------------------------*/
'metaweblog_api_home' =>
"Tableau de bord",

'api_urls' =>
"Voir les URLs pour l'API MetaWeblog",

'invalid_access' =>
"Accès invalide à l'API Metaweblog. Veuillez vérifier que vous avez utilisé les bons identifiant et mot de passe pour un compte de membre avec les permissions pour publier dans au moins un canal.",

'no_channels_found' =>
"Aucun canal trouvé pour cet utilisateur.",

'invalid_channel' =>
"Sélection de canal invalide",

'invalid_categories' =>
"Sélection de catégorie invalide",

'no_entries_found' =>
"Aucun article trouvé",

'no_entry_found' =>
"Article non trouvé dans la base de données",

'unauthorized_action' =>
"Vous n'êtes pas autorisé à exécuter cette action.",

'invalid_access' =>
"Accès invalide",

'entry_uneditable' =>
"Vous n'avez pas la permission d'éditer cet article. Veuillez vérifier les permissions du groupe de membres",

'no_metaweblog_configs' =>
"Il n'y a actuellement aucune configuration d'API Metaweblog",

'metaweblog_configurations' =>
"Configurations d'API MetaWeblog",

'metaweblog_config_name' =>
"Nom",

'metaweblog_config_url' =>
"URL",

'metaweblog_delete_confirm' =>
"Supprimer les configurations d'API Metaweblog",

'metaweblog_deleted' =>
"Configuration d'API Metaweblog supprimée",

'metaweblogs_deleted' =>
"Configurations d'API Metaweblog supprimées",

"metaweblog_delete_question" =>
"Etes-vous sûr de vouloir supprimer les configurations d'API MetaWeblog sélectionnées ?",

'delete' =>
"Supprimer",

'new_config' =>
"Nouvelle configuration",

'modify_config' =>
"Modifier la configuration",

'configuration_options' =>
"Options de configuration",

'metaweblog_pref_name' =>
"Nom de la configuration",

'metaweblog_parse_type' => 
"Préférence de formatage de texte",

'metaweblog_parse_type_subtext' =>
"Paramétrer sur 'oui' affichera le contenu d'un article comme s'il était affiché sur une page web avec tout le BBCode et les répertoires de fichier interprétés.",

'yes' =>
"Oui",

'no' =>
"Non",

'none' =>
"Aucun",

'metaweblog_field_group' =>
"Groupe de champ de canal",

'metaweblog_excerpt_field' =>
"Champ extrait",

'metaweblog_content_field' =>
"Champ contenu",

'metaweblog_more_field' =>
"Plus de champs",

'metaweblog_keywords_field' =>
"Champ mots clés",


'metaweblog_mising_fields' =>
"Un champ n'a pas été rempli",

'configuration_created' =>
"Configuration créée",

'configuration_updated' =>
"Configuration mise à jour",

'metaweblog_create_new' =>
"Créer une nouvelle configuration",

'unable_to_upload' =>
"Impossible de télécharger le fichier",

'metaweblog_upload_dir' =>
"Répertoire de téléchargement pour l'envoi de fichiers",

'metaweblog_upload_dir_subtext' =>
"La plupart des éditeurs de canal n'autorisent pas cette option et vous ne souhaitez peut-être pas autoriser le téléchargement à travers l'API, paramétrez sur 'Non' empêchera ainsi tout téléchargement de fichier via l'éditeur de canal.",

'metaweblog_entry_status' =>
"Statut de l'article",

'do_not_set_status' =>
"Ne pas définir de statut (Le client décide)",

'auto_br' =>
'Auto &lt;br /&gt;',

'xhtml' =>
'XHTML',


''=>''
);

/* End of file lang.metaweblog_api.php */
/* Location: ./system/expressionengine/language/french/lang.metaweblog_api.php */