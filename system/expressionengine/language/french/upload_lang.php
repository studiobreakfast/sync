<?php

$lang = array(

"rotate_unsupported" =>
"La rotation d'image ne semble pas être supportée par votre serveur.",

"nonexistent_filepath" =>
"Impossible de localiser l'image spécifiée.",

"path_does_not_exist" => 
"Le chemin spécifié dans vos préférences de téléchargement est incorrect",

"file_exceeds_ini_limit" =>
"Le fichier téléchargé dépasse la taille maximale définie dans le fichier de configuration PHP",

"file_partially_uploaded" =>
"Le fichier n'a été que partiellement téléchargé",

"no_file_selected" =>
"Vous n'avez pas sélectionné de fichier à télécharger",

"file_upload_error" =>
"Une erreur de type inconnu est survenue.",

"invalid_filetype" =>
"Le type du fichier que vous tentez de télécharger n'est pas autorisé",

"invalid_filesize" =>
"La taille du fichier que vous tentez de télécharger est supérieur à la taille autorisé",

"invalid_dimensions" =>
"Les dimensions de L'image que vous tentez de télécharger sont supérieures à la hauteur ou la largeur maximale autorisée",

"upload_error" =>
"Une erreur a été rencontrée lors du téléchargement du fichier",

"copy_error" =>
"Une erreur a été rencontrée lors du remplacement du fichier. Veuillez vous assurer que le répertoire du fichier est accessible en écriture.",

"unsupported_protocol" =>
"Votre serveur ne supporte pas le protocole de redimensionnement d'image spécifié dans vos préférences.",

"jpg_or_png_required" =>
"Le protocole de redimensionnement d'image spécifié dans vos préférences ne fonctionne qu'avec des images de type JPEG ou PNG.",

"jpg_gif_png_required" =>
"Le protocole de redimensionnement d'image spécifié dans vos préférences ne fonctionne qu'avec des images de type JPEG, GIF ou PNG.",

"libpath_invalid" =>
"Le chemin vers votre bibliothèque d'image est incorrect. Veuillez paramétrer un chemin correct dans vos préférences d'image.",

"image_resize_failed" =>
"Echec du redimensionnement d'image. Veuillez vérifier que votre serveur supporte le protocole choisi et que le chemin vers votre bibliothèque d'image est correct.",

"invalid_filecontent" =>
"Le fichier que vous tentez de télécharger a un contenu invalide par rapport à son type MIME.",


// IGNORE
''=>'');
/* End of file lang.upload.php */
/* Location: ./system/expressionengine/language/french/lang.upload.php */