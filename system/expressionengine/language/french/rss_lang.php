<?php

$lang = array(

//----------------------------------------
// Required for MODULES page
//----------------------------------------

"rss_module_name" =>
"RSS",

"rss_module_description" =>
"Module de génération de page RSS",

//----------------------------------------

"rss_invalid_channel" =>
"Le canal spécifié dans votre flux RSS n'existe pas.",


''=>''
);

/* End of file lang.rss.php */
/* Location: ./system/expressionengine/language/french/lang.rss.php */