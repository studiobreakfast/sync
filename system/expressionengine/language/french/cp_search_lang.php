<?php

$lang = array(

// Design
'design_user_message' =>
"Modèle de message utilisateur",

'design_system_offline' =>
"Modèle du système arrêté",

'design_email_notification' =>
"Modèle de notification email",

'design_member_profile_templates' =>
"Modèle de profil de membre",

// Members
'member_register_member' =>
"Inscrire un membre",

'member_validation' =>
"Validation de membre",

'member_view_members' =>
"Voir les membres",

'member_ip_search' =>
"Rechercher l'adresse IP d'un membre",

'member_custom_profile_fields' =>
"Champs personnalisés de profil",

'member_group_manager' =>
"Gestionnaire de groupe de membres",

'member_config' =>
"Configuration des membres",

'member_banning' =>
"Exclusion de membre",

'member_search' =>
"Recherche de membre",

// Tools_data
'data_sql_manager' =>
"Gestionnaire SQL",

'data_search_and_replace' =>
"Rechercher et remplacer",

'data_recount_stats' =>
"Recompter les statistiques",

'data_php_info' =>
"Informations PHP",

'data_pruning' =>
"Purge des données",

'data_clear_caching' =>
"Vider le cache",

// Tools_files
'file_index' =>
"Gestionnaire de fichier",

// Tools_logs
'logs_view_cp_log' =>
'Voir le journal du tableau de bord',

'logs_view_throttle_log' =>
'Voir le journal du filtrage',

'logs_view_search_log' =>
'Voir le journal de la recherche',

'logs_view_email_log' =>
'Voir le journal d\'email',

// Tools_utilities
'util_member_import' =>
"Importation de membre",

'util_import_from_mt' =>
"Importer depuis Movable Type",

'util_import_from_xml' =>
"Importer depuis un fichier XML",

'util_translation_tool' =>
"Utilitaire de traduction",

''=>''
);

/* End of file lang.cp_search.php */
/* Location: ./system/expressionengine/language/french/lang.cp_search.php */