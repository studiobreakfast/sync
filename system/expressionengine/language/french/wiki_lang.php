<?php

$lang = array(

//----------------------------------------
// Required for MODULES page
//----------------------------------------

"wiki_module_name" =>
"Wiki",

"wiki_module_description" =>
"Un puissant module de Wiki intégré",

//----------------------------------------

'wiki_preferences' =>
"Préférences du module Wiki",

'preference_name' =>
"Nom de la préférence",

"preference_value" =>
"Valeur de la préférence",

'use_captchas' =>
"Utiliser les CAPTCHAs ?",

'text_format' =>
"Formatage de texte pour les articles",

'html_format' =>
"Formatage HTML pour les articles",

'upload_dir' =>
"Répertoire de téléchargement de fichier pour le Wiki",

'upload_url' =>
"URL du répertoire de téléchargement",

"convert_to_entities" =>
"Convertir le HTML en entités de caractères",

"allow_safe_html" =>
"Autoriser uniquement le HTML sûr",

"allow_all_html" =>
"Autoriser TOUT LE HTML",

'auto_br' =>
"Auto &lt;br /&gt;",

'xhtml' =>
'XHTML',

'none' =>
"Aucun",

'update_successful' =>
"Préférences mises à jour avec succès !",

'revision_limit' =>
"Nombre de révisions à conserver par article",

'author_limit' =>
"Nombre d'éditions autorisées par auteur et par jour",

"search_min_length" =>
"Les termes de la recherche doivent faire au moins %x de long",

'invalid_permissions' =>
"Vous n'êtes pas autorisé à exécuter cette action",

'submission_limit' =>
"Limite de soumission dépassée",

'file_exists' =>
"Le fichier existe déjà, veuillez essayer un nouveau nom",

'moderation_emails' =>
"Adresses email pour les notifications de modération",

'namespaces_list' =>
"Liste des espaces de noms",

'namespaces_list_subtext' =>
"Les espaces de noms sont utilisés pour séparer le contenu d'un wiki en plusieurs sections. Par exemple, vous pouvez souhaiter vouloir créer une section
du wiki uniquement pour les articles en espagnol. Pour chaque espace de noms, créez un intitulé, qui sera affiché sur les pages Wiki, un nom court, qui 
sera utilisé en interne et dans certains formulaires pour référencer l'espace de noms. Votre nom court ne doit contenir que des caractères de mots comme
des lettres, des nombres et des underscores.",

'label_name' =>
"Nom complet du Wiki",

'short_name' =>
"Nom court du Wiki",

'name_short_subtext' =>
" - mot unique, sans espaces",

'basepath_unset' =>
"Paramètre du chemin de base non défini",

'users' =>
"Groupes d'utilisateurs",

'admins' =>
"Groupes d'administrateurs",

'wiki_homepage' =>
"Page d'accueil du Wiki",

'create_wiki' =>
"Créer Wiki",

'wiki_created' =>
"Le Wiki a été créé. Vous pouvez maintenant modifier les préférences.",

'no_wiki' =>
"Aucun Wiki n'existe pour le moment.",

'wiki_delete_confirm' =>
"Supprimer les Wikis",

'wiki_delete_question' =>
"Etes-vous sûr de vouloir supprimer le(s) Wiki(s) sélectionné(s) ?",

'wiki_deleted' =>
"Wiki supprimé !",

'wikis_deleted' =>
"Wikis supprimés !",

'duplicate_short_name' =>
"Un autre Wiki utilisant ce nom court existe déjà.",

'default_index_note' => 
"Création de la première page",

'default_index_content' => 
"Bienvenue sur la page d'introduction de votre Wiki EE !",

'namespace_label' =>
"Intitulé de l'espace de noms",

'namespace_short_name' =>
"Nom court de l'espace de noms",

'file_ns' =>
"Fichier",

'image_ns' =>
"Image",

'special_ns' =>
"Spécial",

'category_ns' =>
"Categorie",

'invalid_namespace' =>
"Espace de noms invalide",

'namespaces' =>
"Espaces de noms",

'no_search_terms' =>
'Vous devez inclure des termes de recherche lors de la recherche d\'un espace de noms.',

'duplicate_article' =>
'Un article portant ce nom existe déjà.',

'search_in_wikis' =>
"Rechercher dans les Wikis",

'wikis' =>
"Wikis",

'any_wiki' =>
"Tous les Wikis",

'any_namespace' =>
"Tous les espaces de noms",

'main_ns' =>
"Principal",

'wiki_themes' =>
'Thèmes du Wiki',

'unable_to_find_themes' =>
'Aucun thème du Wiki trouvé',

'invalid_wiki_theme' =>
'Thème du Wiki invalide',

'invalid_wiki_template' =>
'Nom de modèle Wiki invalide',

'wiki_theme_templates' =>
'Modèles de Thème du Wiki',

'edit_template' =>
'Editer le modèle Wiki',

'unable_to_find_template_file' =>
'Impossible de localiser le fichier ',

'template_updated' =>
'Modèle mis à jour avec succès',

'update_and_return' =>
'Mettre à jour et terminer',

"file_not_writable" =>
"Note : ce fichier est inaccessible en écriture",

"error_opening_template" =>
"Erreur : Impossible d'ouvrir le fichier modèle pour y écrire son contenu.",

"file_writing_instructions" =>
"Vous ne pourrez pas sauvegarder les modifications de ce modèle avant qu'il lui soit donné les permissions en écriture pour ce serveur.",

''=>''
);

/* End of file lang.wiki.php */
/* Location: ./system/expressionengine/french/english/lang.wiki.php */