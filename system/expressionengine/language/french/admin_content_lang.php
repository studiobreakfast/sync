<?php

$lang = array(

"reserved_word" =>
"Le nom de champ que vous avez choisi est réservé et ne peut être utilisé. Veuillez vous reporter au guide d'utilisation pour plus d'information.",

"list_edit_warning" =>
"Si vous n'avez pas enregistré les changements sur cette page ils seront perdus lorsque vous serez envoyé vers l'éditeur de formatage.",

"fmt_has_changed" =>
"Note : Vous avez sélectionné un choix de formatage de champ différent de celui sauvegardé précedemment.",

"update_existing_fields" =>
"Mettre à jour tous les articles avec ce nouveau choix de formatage ?",

"display_criteria" =>
"Sélectionnez les critères d'affichage de la page PUBLIER",

"field_type_options" =>
"Option du groupe de champ personnalisé",

"limit" =>
"limite",

"orderby_title" =>
"Trier par Titre",

"orderby_date" =>
"Trier par Date",

"sort_desc" =>
"Ordre Descendant",

"in" =>
"dans",

"sort_asc" =>
"Ordre Ascendant",

"field_label_info" =>
"Ceci est le nom qui apparaîtra sur la page PUBLIER",

"deft_field_formatting" =>
"Formatage de texte par défaut pour ce champ",

"formatting_no_available" =>
"Le formatage de texte est indisponible pour le type de champ choisi",

"show_formatting_buttons" =>
"Afficher les boutons de formatage",

"hide_formatting_buttons" =>
"Masquer les boutons de formatage",

"date_field" =>
"Champ Date",

"relationship" =>
"Relation",

"related_to_channel" =>
"Relier des articles d'un canal à ceux d'un autre canal",

"related_to_gallery" =>
"Relier des articles d'un canal à des articles de galerie",

"select_related_channel" =>
"Sélectionnez le canal avec lequel vous souhaitez établir un lien :",

"select_related_gallery" =>
"Sélectionnez la galerie avec laquelle vous souhaitez établir un lien ",

"gallery_not_installed" =>
"Le module de galerie n'est actuellement pas installé.",

"rss_url" =>
"URL du flux RSS",

"rss_url_exp" =>
"L'adresse à laquelle le flux RSS de ce canal peut être consulté.",

"update_publish_cats" =>
"Fermer la fenêtre et mettre à jour les catégories de la page PUBLIER",

"versioning" =>
"Préférences des révisions",

"enable_versioning" =>
"Activer les versions d'articles",

"clear_versioning_data" =>
"Supprimer toutes les données de version de ce canal",

"max_revisions" =>
"Nombre maximum de révisions récentes par article",

"max_revisions_note" =>
"Les révisions peuvent utiliser un espace considérable de la base de données, il est donc fortement recommandé de limiter leur nombre.",

"field_populate_manually" =>
"Remplir le menu manuellement",

"field_populate_from_channel" =>
"Remplir le menu depuis un autre champ personnalisé",

"select_channel_for_field" =>
"Sélectionnez le champ source pour le remplissage :",

"field_val" =>
"Vous devez choisir un nom de champ de ce menu, pas un nom de canal.",

"channel_notify" =>
"Activer la liste de destination ci-dessous pour la notification des nouveaux articles ?",

"no_statuses" =>
"Aucun statut n'a été trouvé",

"status_created" =>
"Le statut a été créé",

"field_is_hidden" =>
"Afficher ce champ par défaut ?",

"hidden_field_blurb" =>
"Cette préférence détermine si le champ est visible sur la page PUBLIER. Si elle est paramétrée sur \"non\" un lien permettant d'ouvrir le champ sera affiché à la place.",

"include_rss_templates" =>
"Inclure les modèles RSS",

"notification_settings" =>
"Préférences de notification",

"comment_notify_authors" =>
"Prévenir l'auteur d'un article lorsqu'un commentaire est publié ?",

"comment_notify" =>
"Activer la liste des destinataires ci-dessous pour la notification de commentaire ?",

"update_existing_comments" =>
"Mettre à jour tous les commentaires avec ce paramètre d'expiration ?",

"category_order_confirm_text" =>
"Etes-vous sûr de vouloir trier ce groupe de catégorie par ordre alphabétique ?",

"category_sort_warning" =>
"Si vous utilisez un ordre de tri personnalisé, il sera remplacé par un classement alphabétique.",

"global_sort_order" =>
"Ordre de tri prioritaire",

"custom" =>
"Personnalisé",

"alpha" =>
"Alphabétique",

'id' =>
'ID',

"channel_id" =>
"ID",

"channel_short_name" =>
"Nom court",

"group_required" =>
"Vous devez spécifier un nom de groupe.",

"comment_url" =>
"URL de la page commentaire",

"comment_url_exp" =>
"L'adresse à laquelle est située la page commentaire de ce canal",

"order" =>
"Ordre",

"delete_category_confirmation" =>
"Etes-vous sûr de vouloir supprimer la catégorie suivante ?",

"category_description" =>
"Description de la catégorie",

"category_updated" =>
"Catégorie mise à jour",

"new_category" =>
"Créer une nouvelle catégorie",

"template_creation" =>
"Créer de nouveaux modèles pour ce canal ?",

"use_a_theme" =>
"Utiliser un des modèles par défaut",

"myaccount_cp_theme" => 
"Thème du tableau de bord",

"duplicate_group" =>
"Dupliquer un groupe de modèles existant",

"template_group_name" =>
"Nom du nouveau groupe de modèles",

"template_group_choose" =>
"Veuillez choisir un groupe de modèles pour votre nouveau modèle",  //  for your new Template

"new_group_instructions" =>
"Ce champ est requis si vous créez un nouveau groupe",

"publish_page_customization" =>
"Personnalisation de la page PUBLIER",

"show_url_title" =>
"Afficher le champ Titre URL",

"show_button_cluster" =>
"Afficher les boutons de formatage",

"show_author_menu" =>
"Afficher le menu Auteur",

"show_status_menu" =>
"Afficher le menu Statut",

"show_options_cluster" =>
"Afficher les boutons d'options",

"show_ping_cluster" =>
"Afficher les boutons de Ping",

"show_forum_cluster" =>
"Afficher le champ de soumission de sujet de forum",

"show_date_menu" =>
"Afficher les champs Date",

"show_categories_menu" =>
"Afficher le menu de catégories",

"paths" =>
"Paramètres de chemin",

"channel_url_exp" =>
"L'URL de ce canal en particulier",

"search_results_url" =>
"L'URL des résultats de recherche",

"search_results_url_exp" =>
"L'adresse vers laquelle les résultats de la recherche venant de ce canal doivent pointer.",

"ping_return_url" =>
"URL de retour de Ping",

"ping_return_url_exp" =>
"L'adresse que vous souhaitez associer aux liens de trackback sur d'autres sites.",

"comment_expiration" =>
"Expiration des commentaires",

"comment_expiration_desc" =>
"Durée en jours pendant lesquels il est possible de commenter un article. Entrez 0 (zero) si vous ne voulez pas limiter.",

"restrict_status_to_group" =>
"Restreindre le statut pour certains groupes de membres",

"no_publishing_groups" =>
"Aucun groupe de membre autorisé à publier n'est disponible",

"status_updated" =>
"Le statut a été mis à jour",

"status_deleted" =>
"Le statut a été supprimé",

"can_edit_status" =>
"Peut accéder au statut",

"search_excerpt" =>
"Quel champ doit être utilisé pour l'extraction de recherche ?",

"channel_prefs" => 
"Préférences du canal",

"channel_settings" =>
"Préférences de publication du canal",

"comment_prefs" => 
"Préférences de publication de commentaire",

"comment_use_captcha" =>
"Activer Captcha pour la publication de commentaire ?",

"comment_moderate" =>
"Modérer les commentaires ?",

"comment_moderate_exp" =>
"Si paramétré sur oui, les commentaires ne seront visibles qu'après approbation par un modérateur.",

"comment_system_enabled" =>
"Autoriser les commentaires dans ce canal ?",

"edit_channel_prefs" => 
"Editer les préférences du canal",

"edit_group_prefs" => 
"Editer les préférences de groupe",

"edit_group_assignments" => 
"Editer les groupes",

"duplicate_channel_prefs" =>
"Dupliques les préférences du canal",

"do_not_duplicate" =>
'Ne pas dupliquer',

"no_channels_exist" => 
"Il n'y a actuellement aucun canal",

"create_new_channel" => 
"Créer un nouveau canal",

"channel_base_setup" =>
"Préférences générales du canal",

"default_settings" =>
"Préférences d'administration",

"channel_name" => 
"Nom court",

"channel_url" =>
"URL du canal",

"channel_lang" =>
"Langue XML",

"channel_description" =>
"Description du canal",

"illegal_characters" =>
"Le nom doit contenir uniquement des caractères alphanumériques, des espaces, des underscores ou des tirets",

"comment_require_membership" =>
"Requérir une inscription pour publier des commentaires ?",

"channel_require_membership" =>
"Requérir une inscription pour publier des articles ?",

"comment_require_email" =>
"Requérir l'adresse email pour publier des commentaires ?",

"channel_require_email" =>
"Requérir l'adresse email pour publier des articles ?",

"channel_max_chars" =>
"Nombre maximum de caractères autorisés dans les articles",

"comment_max_chars" =>
"Nombre maximum de caractères autorisés dans les commentaires",

"comment_timelock" =>
"Intervalle de temps entre chaque publication de commentaire",

"comment_timelock_desc" =>
"Nombre de secondes avant qu'un utilisateur puisse soumettre un autre commentaire. Laisser vide ou saisir 0 pour ne pas limiter.",

"comment_text_formatting" =>
"Formatage du texte des commentaires",

"channel_text_formatting" =>
"Formatage par défaut du texte des articles",

"comment_html_formatting" =>
"Formatage HTML des commentaires",

"channel_html_formatting" =>
"Formatage HTML par défaut des articles",

"convert_to_entities" =>
"Convertir le HTML en entité de caractères",

"allow_safe_html" =>
"Autoriser le HTML sûr uniquement",

"allow_all_html" =>
"Autoriser tout le HTML",

"allow_all_html_not_recommended" =>
"Autoriser tout le HTML (déconseillé)",

"comment_notify_note" =>
"Séparer les adresses par une virgule",

"comment_notify_emails" =>
"Adresse(s) email du(des) destinataire(s) de la notification",

"comment_allow_img_urls" =>
"Autoriser les URLs d'image dans les commentaires ?",

"channel_allow_img_urls" =>
"Autoriser les URLs d'image dans les articles ?",

"auto_link_urls" =>
"Convertir automatiquement URL et adresses email en lien ?",

"single_word_no_spaces" => 
"Un seul mot, sans espaces",

"channel_title" => 
"Nom complet du canal",

"edit_channel" => 
"Editer le canal",

"channel_name" => 
"Nom du canal",

"new_channel" => 
"Nouveau canal",

"channel_created" => 
"Canal créé :",

"channel_updated" => 
"Canal mis à jour :",

"taken_channel_name" => 
"Ce nom de canal est déjà utilisé.",

"no_channel_name" => 
"Vous devez donner à votre canal un nom \"court\".",

"no_channel_title" =>
"Vous devez donner à votre canal un nom \"complet\".",

"invalid_short_name" => 
"Le nom de votre canal ne doit contenir que des caractères alphanumériques et pas d'espaces.",

"delete_channel" => 
"Supprimer le canal",

"channel_deleted" => 
"Canal supprimé :",

"delete_channel_confirmation" =>
"Etes-vous sûr de vouloir définitivement supprimer ce canal ?",

"be_careful" =>
"ATTENTION !",

"action_can_not_be_undone" =>
"CETTE ACTION NE PEUT ÊTRE ANNULÉE",

"assign_group_to_channel" =>
"Note : Afin d'utiliser votre nouveau groupe, vous devez l'affecter à un canal.",

"click_to_assign_group" =>
"Cliquez ici pour l'affecter",

"default" =>
"Défaut",

"category" =>
"Catégorie",

"deft_status" =>
"Statut par défaut",

"deft_category" =>
"Catégorie par défaut",

"deft_comments" =>
"Cocher \"Autoriser les commentaires\" par défaut dans la page de publication ?",

"no_field_group_selected" =>
"Aucun groupe de champ disponible pour ce canal",

"invalid_field_group_selected" =>
"Groupe de champ invalide",

"missing_channel_data_for_pings" =>
"Afin d'envoyer des pings, votre canal nécessite un titre et une URL. Veuillez mettre à jour vos préférences de canal.",

"open" => 
"Ouvert",

"closed" => 
"Fermé",

"none" =>
"Aucun",

"define_html_buttons" =>
"Définitions des boutons de formatage HTML",

'no_buttons' => 
'Aucun bouton HTML n\'est défini.',

"htmlbutton_delete_instructions" =>
"Pour supprimer un élément, soumettez le formulaire en laissant le nom de balise vide",

"tag_name" =>
"Nom de la balise",

"tag_open" =>
"Balise d'ouverture",

"tag_close" =>
"Balise de fermeture",

"accesskey" =>
"Raccourci",

"tag_order" =>
"Ordre",

"row" =>
"Ligne",

"server_name" =>
"Nom du serveur",

"server_url" =>
"URL/Chemin du serveur",

"port" =>
"Port",

"protocol" =>
"Protocole",

"is_default" =>
"Défaut",

"server_order" =>
"Ordre",

"define_ping_servers" =>
"Ces formulaires vous permettent de définir une liste de serveurs qui recevront un ping lors de la publication de nouveaux articles",

"pingserver_delete_instructions" =>
"Pour supprimer un élément, soumettez le formulaire en laissant le nom de serveur vide",

"assign_channels" =>
"Choisissez à quel(s) canal(canaux) vous souhaitez assigner ce groupe",

//----------------------------
// Category Administration
//----------------------------

"category_group" =>
"Groupe de catégorie",

"category_groups" =>
"Groupes de catégorie",

"no_category_group_message" =>
"Il n'y a actuellement aucune catégorie",

"no_category_message" =>
"Il n'y a actuellement aucune catégorie assigné à ce groupe",

"create_new_category_group" =>
"Créer un nouveau groupe de catégorie",

"edit_category_group" =>
"Editer le groupe de catégorie",

"name_of_category_group" =>
"Nom du groupe de catégorie",

"taken_category_group_name" => 
"Ce nom de groupe est déjà utilisé.",

"add_edit_categories" => 
"Ajouter/Editer des catégories",

"edit_group_name" => 
"Editer le groupe",

"delete_group" => 
"Supprimer le groupe",

"category_group_created" => 
"Groupe de catégorie créé :",

"category_group_updated" => 
"Groupe mis à jour:",

"delete_cat_group_confirmation" =>
"Etes-vous sûr de vouloir de supprimer ce groupe de catégorie ?",

"category_group_deleted" => 
"Groupe de catégorie supprimé :",

"create_new_category" =>
"Créer une nouvelle catégorie",

"add_new_category" =>
"Ajouter une nouvelle catégorie",

"edit_category" =>
"Editer la catégorie",

"delete_category" =>
"Supprimer la catégorie",

"category_deleted" =>
"Catégorie supprimée",

'category_url_title' =>
'Titre URL de la catégorie',

'cat_url_title_is_numeric' =>
'Les nombres ne peuvent être utilisés comme titre URL de catégorie',

'unable_to_create_cat_url_title' =>
'Impossible de créer un titre URL valide pour votre catégorie',

'duplicate_cat_url_title' =>
'Une catégorie portant le titre URL spécifié existe déjà dans ce groupe de catégorie',

"category_name" =>
"Nom de la catégorie",

"category_image" =>
"URL de l'image de la catégorie",

"category_img_blurb" =>
"Ce champ optionnel vous permet d'affecter une image à vos catégories.",

"category_parent" =>
"Catégorie parente",

"custom_category_fields" =>
"Champs personnalisés de catégorie",

'manage_custom_fields' =>
'Gérer les champs personnalisés',

'delete_cat_field' =>
'Supprimer le champ de catégorie',

'delete_cat_field_confirmation' =>
'Etes-vous sûr de vouloir supprimer définitivement ce champ de catégorie ?',

'cat_field_deleted' =>
'Champ de catégorie supprimé :',

'cat_field_updated' =>
'Champ de catégorie mis à jour :',

'edit_cat_field' =>
'Editer le champ de catégorie',

'create_new_cat_field' =>
'Créer un nouveau champ de catégorie',

'cat_field_created' =>
'Nouveau champ de catégorie créé : ',

'cat_field_edited' =>
'Champ de catégorie édité :',

'category_created' =>
'Nouvelle catégorie créée',

'category_edited' =>
'Catégorie édité',

'cat_field_label_info' =>
'Ceci est le nom qui apparaîtra sur la apge d\'édition de catégorie',

'update_existing_cat_fields' =>
'Mettre à jour toutes les catégories de ce groupe avec vos nouveaux paramètres de formatage ?',

'formatting' =>
'Formatage :',

'cat_field_html_formatting' =>
'Formatage HTML des champs de catégorie',

'can_edit_categories' =>
'Peut éditer les catégories',

'can_delete_categories' =>
'Peut supprimer des catégories',

'exclude_from_channels_or_publish' =>
'Exclure de ce canal ou d\'un groupe de champs ?',
'exclude_from_publish' 		=> 'Editer le canal',
'exclude_from_files'		=> 'Editer le fichier',

'no_member_groups_available' =>
'Il n\'y a aucun groupe de membres pour les catégories %x. Vous pouvez assigner ces privilèges depuis l\'éditeur de groupe de membres : ',

'member_group' => 
'Groupe de membres',

'member_groups' => 
'Groupes de membres',

'missing_required_fields' =>
'Les champs requis sont manquants :',

//----------------------------
// Custom field Administration
//----------------------------

"field_settings" =>
"Préférences du groupe de champ personnalisé",


"field_group" =>
"Groupe de champ personnalisé",

"field_groups" =>
"Groupe de champs personnalisés",

"field_group_name" =>
"Nom du groupe de champs",

"custom_fields" =>
"Champs personnalisés",

"no_field_group_message" =>
"Il n'y a actuellement aucun champs personnalisés de canal",

"create_new_field_group" =>
"Créer un nouveau champ personnalisé",

"new_field_group" =>
"Nouveau groupe de champ",

"add_edit_fields" => 
"Ajouter/Editer des champs personnalisés",

"edit_field_group_name" =>
"Editer le groupe de champ",

"delete_field_group" =>
"Supprimer le groupe de champ",

"create_new_field" =>
"Créer un nouveau champ",

"edit_field" =>
"Editer le champ",

"custom_field_edited" =>
"Champ personnalisé édité",

"custom_field_created" =>
"Champ personnalisé créé",

"no_field_groups" =>
"Il n'y a aucun champ personnalisé dans ce groupe",

"delete_field" =>
"Supprimer le champ",

"field_deleted" =>
"Champ personnalisé supprimé :",

"edit_field_order" =>
"Ordre des champs",

"create_new_custom_field" =>
"Créer un nouveau champ personnalisé",

"field_id" =>
"ID du champ",

"field_label" =>
"Etiquette du champ",

"field_name" =>
"Nom du champ",

"field_name_cont" =>
"Un seul mot, sans espaces. Underscores et tirets autorisés",

"field_type" =>
"Type de champ",

"field_max_length" =>
"Longueur max.",

"field_max_length_cont" =>
"Si vous utilisez un champ de type \"texte\"",

"textarea_rows" =>
"Nombre de lignes de la zone de texte",

"textarea_rows_cont" =>
"Si vous utilisez un champ de type \"zone de texte\"",

"dropdown_sub" =>
"Si vous utilisez un champ de type \"liste déroulante\"",

"field_list_items" =>
"Options de la liste",

"multi_list_items" =>
"Option de la liste multiple",

"option_group_items" =>
"Options des cases à cocher",

"radio_items" =>
"Options des radios",

"field_list_items_cont" =>
"Si vous avez choisi menu déroulant",

"field_list_instructions" =>
"Mettre chaque choix sur une ligne séparée",

"field_formatting" =>
"Formatage du champ",

"edit_list" =>
"Editer la liste",

"formatting_options" =>
"Options du formatage de champ",

"field_formatting_cont" =>
"Si vous avez choisi le type de champ zone de texte",

"field_order" =>
"Ordre d'affichage des champs",

"is_field_searchable" =>
"Le champ peut-il être recherché ?",

"is_field_required" =>
"Le champ est-il requis ?",

"show_smileys" =>
"Afficher les smileys",

"show_glossary" =>
"Afficher le glossaire",

"show_spellcheck" =>
"Afficher le vérificateur d'orthographe",

"show_file_selector" =>
"Afficher le sélecteur de fichier",

"show_formatting_btns" =>
"Afficher les boutons de formatage",

"show_writemode" =>
"Afficher le mode d'écriture",

"text_input" =>
"Entrée de texte",

"textarea" =>
"Zone de texte",

"file" =>
"Téléchargement",

"select_list" =>
"Liste déroulante",

"multi_select" =>
"Liste multiple",

"option_group" =>
"Case à cocher ",

"radio" =>
"Radio",

"auto_br" =>
"Auto &lt;br /&gt;",

"xhtml" =>
"XHTML",

"no_field_name" =>
"Vous devez saisir un nom de champ",

"no_field_label" =>
"Vous devez saisir une étiquette de champ",

"invalid_characters" =>
"Le nom de champ que vous avez entré contient des caractères invalides",

"custom_field_empty" =>
"Les champs suivants sont requis :",

"duplicate_field_name" =>
"Le nom de champ que vous avez choisi est déjà utilisé",

"taken_field_group_name" =>
"Le nom que vous avez choisi est déjà utilisé",

"field_group_created" =>
"Groupe de champs créé :",

"field_group_updated" =>
"Groupe de champs mis à jour :",

"field_group_deleted" =>
"Groupe de champs supprimé :",

"field_group" =>
"Groupe de champs",

"delete_field_group_confirmation" =>
"Etes-vous sûr de vouloir définitivement supprimer ce groupe de champs personnalisés ?",

"delete_field_confirmation" =>
"Etes-vous sûr de vouloir définitivement supprimer ce champ personnalisé ?",

"channel_entries_will_be_deleted" =>
"Tous les articles de canal contenu dans le(s) champ(s) suivant seront définitivement supprimés.",

'field_content_text' =>
"Contenu du champ :",

'field_content_file' =>
"Type de fichier :",

'type_numeric' =>
"Nombre",

'type_integer' =>
"Entier",

'type_decimal' =>
'Décimal',

'type_file' =>
"Fichier",

'type_image' =>
"Image",

//----------------------------
// Status Administration
//----------------------------

"status_group" =>
"Groupe de statuts",

"status_groups" =>
"Groupes de statuts",

"no_status_group_message" =>
"Il n'y a actuellement aucun statuts personnalisés",

"create_new_status_group" =>
"Créer un nouveau groupe de statuts",

"edit_status_group" =>
"Editer le groupe de statuts",

"name_of_status_group" =>
"Nom du groupe de statuts",

"taken_status_group_name" => 
"Ce nom de groupe de statuts est déjà utilisé.",

"invalid_status_name" => 
"Les noms de statuts peuvent uniquement contenir des caractères alphanumériques, des espaces, des underscores ou des tirets.",

"duplicate_status_name" =>
"Un statut portant le même nom existe déjà.",

"status_group_created" => 
"Groupe de statuts créé :",

"new_status" => 
"Nouveau statut",

"status_group_updated" => 
"Groupe de statuts mis à jour :",

"add_edit_statuses" => 
"Ajouter/Editer les statuts",

"edit_status_group_name" => 
"Editer le groupe de statuts",

"delete_status_group" => 
"Supprimer le groupe de statuts",

"delete_status_group_confirmation" =>
"Etes-vous sûr de vouloir définitivement supprimer ce groupe de statuts ?",

"status_group_deleted" => 
"Groupe de statuts supprimé :",

"create_new_status" => 
"Créer un nouveau statut",

"status_name" => 
"Nom du statut",

"status_order" =>
"Ordre du statut",

"change_status_order" =>
"Changer l'ordre des statuts",

"highlight" =>
"Couleur (optionnel)",

"statuses" => 
"Statuts",

"edit_status" =>
"Editer le statut",

"delete_status" =>
"Supprimer le statut",

"delete_status_confirmation" =>
"Etes-vous sûr de vouloir définitivement supprimer le statut suivant ?",

'url_title_prefix' =>
"Préfixe du titre URL",

'live_look_template' =>
'Modèle d\'aperçu rapide',

'no_live_look_template' =>
'- Aucun modèle d\'aperçu rapide -',

'default_entry_title' =>
"Titre d'article par défaut",

'invalid_url_title_prefix' =>
"Préfixe du titre URL invalide",

'multiple_cat_group_preferences' =>
"Préférences du groupe de catégories multiples",

'integrate_category_groups' =>
"Intégrer les groupes de catégories",

'text_direction' =>
"Sens du texte",

'ltr' =>
"De gauche à droite",

"rtl" =>
"De droite à gauche",

"direction_unavailable" =>
"Le sens du texte n'est pas disponible pour le type de champ choisi",

'field_instructions' =>
"Instructions du champ",

'field_instructions_info' =>
"Instructions pour les auteurs concernant la méthode de saisie et lecontenu pour le champ personnalisé lors de la soumission d'un article.",

"show_pages_cluster" =>
"Afficher les champs de soumission de Pages",


//----------------------------
// Channel Entries API
//----------------------------

'invalid_api_parameter' =>
"Impossible de créer/mettre à jour l'article. Les paramètres de l'API sont manquants.",


''=>''
);

/* End of file lang.admin_content.php */
/* Location: ./system/expressionengine/language/french/lang.admin_content.php */