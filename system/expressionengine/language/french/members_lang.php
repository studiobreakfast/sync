<?php
$lang = array(


'register_member' => 
'Inscrire un nouveau membre',

'view_members' => 
'Voir les membres',

'view_search_members' => 
'Voir / Rechercher un membre',

'member_validation' => 
'Activer les membres en attente',

'member_search' => 
'Recherche de membre',

'user_banning' => 
'Exclusion d\'utilisateur',

'custom_profile_fields' => 
'Champs de membres personnalisés',

'member_cfg' => 
'Préférences d\'adhésion',

'preferences_updated' => 
'Préférences mises à jour',

'profile_templates' => 
'Modèles de profil de membre',

'ip_search' => 
'Recherche d\'adresse IP',

'member_groups' => 
'Groupes de membre',

'keywords' => 
'Mot-clés',

'filter_by' => 
'Filtré par',

'filter_member_name' => 
'Filtrer par nom de membre',

'filter_member_group' => 
'Filtrer par groupe de membre',

'member_group_assignment' => 
'Assignation de groupe de membre',

'password_confirm' => 
'Confirmation du mot de passe',

'email' => 
'Email',

'invalid_path' => 
'Le chemin que vous avez saisi est invalide.',

'invalid_path_description' => 
'Le chemin suivant est invalide :',

'not_writable_path' => 
'Le chemin que vous avez saisi est inaccessible en écriture. Veuillez vérifier que les permissions de fichier sont paramétrées sur 777.',

'include_in_memberlist' => 
'Inclure les membres dans la liste des membres du site ?',

'include_in_authorlist' => 
'Inclure les membres dans la liste multi-auteurs de la page PUBLIER ?',

'field_description' => 
'Description du champ',

'field_description_info' => 
'Ce champ peut être utilisé pour fournir des instructions ou une information additionnelle.',

'can_edit_categories' => 
'Peut éditer et ajouter de nouvelles catégories',

'can_delete_categories' => 
'Peut supprimer des catégories',

'can_not_delete_self' => 
'Vous n\'êtes pas autorisé à supprimer votre propre compte membre.',

'no_heirs_available' => 
'Le membre que vous êtes sur le point de supprimer a des articles de canal qui lui sont assignés',

'heir_to_member_entries' => 
'Ce membre a des articles assignés à son compte. Choisissez un membre auquel réassigner ces articles.',

'heir_to_members_entries' => 
'Certains de ces membres ont des articles assignés à leurs comptes. Choisissez un membre auquel réassigner ces articles.',

'forum_topics' => 
'Sujets de forum',

'forum_posts' => 
'Messages de forum',

'topic' => 
'Sujet',

'author' => 
'Auteur',

'comments' => 
'Commentaires',

'channel_entries' => 
'Articles de canal',

'ip_search_instructions' => 
'Vous pouvez soumettre une adresse IP partielle ou complète',

'ip_search_too_short' => 
'L\'adresse IP que vous avez saisie est trop courte. Elle doit faire au moins 3 caractères de long.',

'ip_search_no_results' => 
'Votre recherche n\'a retourné aucun résultat',

'member_accounts' => 
'Comptes de membres',

'login_as_user' => 
'Super Administrateur connexion comme utilisateur',

'control_panel' => 
'Tableau de bord',

'site_homepage' => 
'Page d\'accueil du site',

'login_as_member_description' => 
'Lorsque vous cliquerez sur le bouton valider, vous serez connecté comme le membre \'%screen_name%\' et redirigé à la page ci-dessous.',

'other' => 
'Autre',

'field_order_updated' => 
'Ordre des champs mis à jour',

'priv_msg_privs' => 
'Privilèges de messagerie privée',

'can_send_private_messages' => 
'Peut envoyer des messages privés',

'can_attach_in_private_messages' => 
'Peut ajouter des pièces jointes aux messages privés',

'fieldlabel' => 
'Intitulé du champ',

'index' => 
'Page Index principale',

'member_assignment_warning' => 
'%x membres(s) sont actuellement assignés à ce groupe. A quel groupe de membre doivent-ils être ré-assignés ?',

'can_moderate_comments' => 
'Peut modérer les commentaires',

'exclude_from_moderation' => 
'Exclure le membre de la modération de commentaires',

'total_members' => 
'Nombre total de membres :',

'admin_members' => 
'Membres administrateurs',

'validate_selected' => 
'Activer les membres sélectionnés',

'delete_selected' => 
'Supprimer les membres sélectionnés',

'send_email_notification' => 
'Envoyer un email de notification à chaque personne',

'your_account_validated' => 
'Votre compte a été activé.',

'your_account_ready' => 
'Votre compte de membre a été activé et est prêt à être utilisé.',

'thank_you' => 
'Merci !',

'no_members_to_validate' => 
'Il n\'y a aucun compte de membre en attente d\'activation',

'members_are_validated' => 
'Membre(s) ont été activé(s)',

'members_are_deleted' => 
'Membre(s) ont été supprimé(s)',

'ascending' => 
'Date - Plus ancien en premier',

'descending' => 
'Date - Plus récent en premier',

'alpha' => 
'Ordre alphabétique ascendant',

'alpha_desc' => 
'Ordre alphabétique descendant',

'username_asc' => 
'Nom d\'utilisateur - ascendant',

'username_desc' => 
'Nom d\'utilisateur - descendant',

'screen_name' => 
'Nom à l\'écran',

'screen_name_asc' => 
'Nom à l\'écran - ascendant',

'screen_name_desc' => 
'Nom à l\'écran - descendant',

'mbr_email_address' => 
'Adresse email',

'email_address' => 
'Adresse email',

'email_asc' => 
'Email - ascendant',

'email_desc' => 
'Email - descendant',

'sort_order' => 
'Ordre de tri',

'join_date' => 
'Date d\'adhésion',

'last_visit' => 
'Dernière visite',

'member_group' => 
'Groupe de membre',

'group_id' => 
'ID de groupe',

'no_members_matching_that_criteria' => 
'Il n\'y a aucun membre correspondant aux critères spécifiés',

'mbrs' => 
'Membres',

'edit_profile' => 
'Editer le profil',

'edit_group' => 
'Editer le groupe',

'group_title' => 
'Titre du groupe',

'security_lock' => 
'Verrou de sécurité',

'group_lock' => 
'Verrou de sécurité du groupe de membre',

'enable_lock' => 
'Activer le verrou de sécurité du groupe',

'lock_description' => 
'Quand un groupe est verrouillé, seul un Super Administrateur peut y assigner un membre. Si vous autorisez d\'autres utilisateurs à administrer les comptes de membre, il est vivement recommandé que vous déverrouilliez UNIQUEMENT les groupes auxquels ils pourront assigner des utilisateurs.',

'locked' => 
'Verrouillé',

'unlocked' => 
'Déverrouillé',

'create_new_member_group' => 
'Créer un nouveau groupe de membre',

'member_has_cp_access' => 
'Peut accéder au tableau de bord',

'edit_member_group' => 
'Editer groupe de membre',

'create_member_group' => 
'Créer un groupe de membre',

'delete_member_group' => 
'Supprimer un groupe de membre',

'member_group_deleted' => 
'Groupe de membre supprimé',

'delete_member_group_confirm' => 
'Etes-vous sûr de vouloir supprimer ce groupe de membre ?',

'delete_member' => 
'Supprimer le membre',

'delete_member_confirm' => 
'Etes-vous sûr de vouloir supprimer ce membre ?',

'delete_members_confirm' => 
'Etes-vous sûr de vouloir supprimer ces membres ?',

'can_not_delete_superadmin' => 
'Vous ne pouvez pas supprimer les membres principaux',

'member_deleted' => 
'Membre supprimé',

'members_deleted' => 
'Membres supprimés',

'ban_preferences_updated' => 
'Préférences d\'exclusion mise à jour',

'ip_address_banning' => 
'Adresses IP exclues',

'ip_banning_instructions' => 
'Placez chaque adresse IP sur une ligne séparée',

'ip_banning_instructions_cont' => 
'Utilisez des jokers pour des adresses IP partielles. Exemple:  123.345.*',

'email_address_banning' => 
'Adresses email exclues',

'email_banning_instructions' => 
'Placez chaque adresse email sur une ligne séparée',

'email_banning_instructions_cont' => 
'Utilisez des jokers pour des adresses email partielles. Exemple:  *@domaine.com',

'ban_options' => 
'Lorsque une adresse IP exclue tente d\'accéder au site',

'restrict_to_viewing' => 
'La restreindre à la consultation uniquement',

'show_this_message' => 
'Afficher ce message',

'send_to_site' => 
'Rediriger vers ce site',

'ban_message' => 
'Ce site est actuellement indisponible',

'username_banning' => 
'Noms d\'utilisateurs réservés',

'username_banning_instructions' => 
'Placez chaque nom d\'utilisateur sur une ligne séparée',

'screen_name_banning' => 
'Noms à l\'écran réservés',

'screen_name_banning_instructions' => 
'Placez chaque nom sur une ligne séparée',

'only_superadmins_can_admin_groups' => 
'Seuls les Super Administrateurs peuvent accéder à la page des groupes de membres',

'create_group_based_on_old' => 
'Créer un nouveau groupe basé sur un groupe existant',

'group_name' => 
'Nom du groupe de membre',

'missing_group_title' => 
'Le groupe de membre doit avoir un nom',

'site_access' => 
'Accès au site',

'can_view_online_system' => 
'Peut voir le site lorsqu\'il est en ligne',

'can_view_offline_system' => 
'Peut voir le site lorsqu\'il est à l\'arrêt',

'mbr_account_privs' => 
'Privilèges de compte de membres',

'prv_msg_storage_limit' => 
'Nombre maximum de messages privés qu\'un utilisateur peut conserver',

'prv_msg_send_limit' => 
'Nombre maximum de messages privés qu\'un utilisateur peut envoyer par jour',

'can_view_profiles' => 
'Peut voir les profils publics',

'can_edit_html_buttons' => 
'Peut éditer ses boutons de design HTML',

'can_delete_self' => 
'Peut supprimer son propre compte - supprime également tous ses messages, articles et commentaires',

'mbr_delete_notify_emails' => 
'Adresses email des destinataires de la notification de suppression',

'delete_confirmation_form' => 
'Formulaire de confirmation de suppression de compte',

'commenting_privs' => 
'Privilèges de publication de commentaire',

'can_post_comments' => 
'Peut publier des commentaires',

'cp_channel_post_privs' => 
'Assignation de canal',

'cp_module_access_privs' => 
'Privilèges d\'accès aux modules',

'no_cp_modules_installed' => 
'Pas de modules avec accès au panneau de contrôle installé',

'can_access_mod' => 
'Peut accéder aux modules :',

'global_cp_access' => 
'Accès au tableau de bord',

'can_access_cp' => 
'Peut accéder au tableau de bord ?',

'can_access_content' => 
'Peut accéder à la page CONTENU',

'can_access_design' => 
'Peut accéder à la page DESIGN',

'can_access_addons' => 
'Peut accéder à la page ADDONS',

'can_access_members' => 
'Peut accéder à la page MEMBRES',

'can_access_admin' => 
'Peut accéder à la page ADMIN',

'can_access_tools' => 
'Peut accéder à la page OUTILS',

'can_ban_users' => 
'Peut exclure des utilisateurs',

'cp_admin_privs' => 
'Administration du tableau de bord',

'cp_comment_privs' => 
'Administration des commentaires',

'can_view_other_comments' => 
'Peut voir les commentaires dans les articles rédigés par d\'autres auteurs',

'can_edit_own_comments' => 
'Peut éditer les commentaires de ses propres articles',

'can_delete_own_comments' => 
'Peut supprimer les commentaires de ses propres articles',

'can_edit_all_comments' => 
'Peut éditer les commentaires de N\'IMPORTE QUEL article',

'can_delete_all_comments' => 
'Peut supprimer les commentaires de N\'IMPORTE QUEL article',

'can_admin_channels' => 
'Peut administrer les canaux et les préférences de canal',

'can_admin_upload_prefs' => 
'Peut administrer les préférences d\'envoi de fichiers',

'can_admin_members' => 
'Peut administrer les comptes de membres',

'can_access_edit' => 
'Peut accéder à la page EDITER',

'can_access_publish' => 
'Peut accéder à la page PUBLIER',

'can_access_files' => 
'Peut accéder à la page GESTIONNAIRE DE FICHIER',

'can_access_modules' => 
'Peut accéder à la page MODULES',

'can_access_extensions' => 
'Peut accéder à la page EXTENSIONS',

'can_access_accessories' => 
'Peut accéder à la page ACCESSOIRES',

'can_access_plugins' => 
'Peut accéder à la page PLUGINS',

'can_access_fieldtypes' => 
'Peut accéder à la page CHAMPS',

'can_access_comm' => 
'Peut accéder à la page COMMUNIQUER',

'can_access_utilities' => 
'Peut accéder à la page OUITLS',

'can_access_data' => 
'Peut accéder à la page DONNEES',

'can_access_logs' => 
'Peut accéder à la page LOGS',

'can_access_sys_prefs' => 
'Peut accéder à la page PREFERENCES SYSTEME',

'can_access_content_prefs' => 
'Peut accéder à la page ADMINISTRATION',

'can_delete_members' => 
'Peut supprimer des membres',

'can_admin_mbr_groups' => 
'Peut changer le groupe auquel un membre est assigné (groupes déverouillés uniquement)',

'can_admin_mbr_templates' => 
'Peut administrer les modèles de profil de membre',

'can_admin_templates' => 
'Peut administrer les modèles et les groupes de modèles',

'can_admin_design' => 
'Peut gérer les préférences de design',

'can_admin_modules' => 
'Peut installer/désinstaller des modules',

'cp_email_privs' => 
'Privilèges d\'email du tableau de bord',

'can_send_email' => 
'Peut envoyer des emails depuis le tableau de bord',

'can_email_member_groups' => 
'Peut envoyer des emails aux groupes de membres',

'can_email_mailinglist' => 
'Peut envoyer des emails à la liste de diffusion',

'can_send_cached_email' => 
'Peut voir/envoyer des emails en cache',

'search_privs' => 
'Privilèges de recherche',

'can_search' => 
'Peut utiliser l\'utilitaire de recherche',

'search_flood_control' => 
'Nombre de secondes entre les recherches',

'warning' => 
'AVERTISSEMENT :',

'super_admin_edit_note' => 
'Note : Vous ne pouvez éditer que le nom et la description du groupe Super Administrateur',

'be_careful_assigning_groups' => 
'Soyez EXTREMEMENT prudent lors de l\'assignation des préférences de groupe - en particulier ceux colorés.',

'member_group_updated' => 
'Groupe de membre mis à jour :',

'member_group_created' => 
'Groupe de membre créé :',

'cp_channel_privs' => 
'Privilèges de publication en canal',

'can_view_other_entries' => 
'Peut voir les articles rédigés par d\'autres auteurs',

'can_post_in' => 
'Peut publier et éditer des articles dans :',

'can_edit_other_entries' => 
'Peut éditer des articles rédigés par d\'autres auteurs',

'can_assign_post_authors' => 
'Peut changer le nom d\'auteur lors de la publication d\'articles',

'can_delete_self_entries' => 
'Peut supprimer ses propres articles',

'can_delete_all_entries' => 
'Peut supprimer des articles rédigés par d\'autres auteurs',

'cp_template_access_privs' => 
'Privilèges d\'édition de modèles',

'can_access_tg' => 
'Peut éditer des modèles dans ce groupe :',

'can_email_from_profile' => 
'Peut envoyer des emails à d\'autres membres depuis la console email du profil',

'for_profile_page' => 
'Sera affiché dans la page de profil de membre',

'custom_member_fields' => 
'Champs de profil personnalisés',

'current_fields' => 
'Champs de profil de membre',

'edit_member_field' => 
'Editer le champ',

'create_member_field' => 
'Créer un champ',

'field_updated' => 
'Champ mis à jour',

'field_created' => 
'Champ créé',

'create_new_profile_field' => 
'Créer un nouveau champ de profil',

'is_field_public' => 
'Ce champ est-il visible dans les profils publics ?',

'is_field_public_cont' => 
'Si vous choisissez "non", il sera visible uniquement par les administrateurs',

'is_field_reg' => 
'Ce champ est-il visible sur la page d\'inscription ?',

'is_field_cp_reg' => 
'Est-ce que le champ est visible dans la page d\'enregistrement administratif du panneau de contrôle ?',

'field_width' => 
'Longueur du champ',

'field_width_cont' => 
'Peut être en pixel ou en pourcent',

'fieldname' => 
'Nom du champ',

'fieldname_cont' => 
'Mot unique, sans espace. Underscores et tirets autorisés',

'field_format' => 
'Formatage de texte',

'm_max_length' => 
'Longueur maximale',

'max_length_cont' => 
'Pour les champs de type texte',

'text_area_rows' => 
'Lignes de la zone de texte',

'text_area_rows_cont' => 
'Pour les champs de type zone de texte',

'pull_down_items' => 
'Options de la liste de sélection',

'pull_down_items_cont' => 
'Pour les menus déroulants',

'pull_down_instructions' => 
'Saisissez chaque élément sur une ligne séparée',

'invalid_characters' => 
'Le nom du champ que vous avez saisi contient des caractères invalides',

'member_data_will_be_deleted' => 
'Toutes les données de membre contenues dans ce champ seront définitivement supprimées',

'profile_field_deleted' => 
'Champ de profil supprimé :',

'no_custom_profile_fields' => 
'Il n\'y a actuellement aucun champ de profil personnalisé',

'member_search_results' => 
'Résultats de la recherche de membre',

'no_search_results' => 
'Votre recherche n\'a retourné aucune correspondance',

'member_search_instructions' => 
'Remplissez au moins un champ. Vous pouvez soumettre une expression partielle ou complète.',

'can_not_delete_only_member' => 
'Vous ne pouvez pas supprimer le seul membre de la base de données',

'can_not_delete_super_admin' => 
'Vous ne pouvez pas supprimer un Super Administrateur tant qu\'il n\'en existe pas au moins un autre',

'must_be_superadmin_to_delete_one' => 
'Vous devez être un Super Administrateur pour supprimer un autre Super Administrateur',

'email_console_log' => 
'Journaux de la console d\'email',

'no_cached_email' => 
'Il n\'y a aucun messages email en cache.',

'email_title' => 
'Titre de l\'email',

'from' => 
'De',

'to' => 
'À',

'message_sent_to' => 
'Message envoyé à :',

'email_deleted' => 
'Message(s) email supprimé(s)',

'can_send_bulletins' => 
'Peut envoyer des bulletins',

'group_description' => 
'Description du groupe de membre',

'preferences' => 
'Préférences',

'preference' => 
'Préférence',

'setting' => 
'Paramètre',

'resend_activation_emails' => 
'Renvoyer les emails d\'activation',

'activation_emails_resent' => 
'Emails d\'activation renvoyés',

'resend_activation_email' => 
'Renvoyer l\'email d\'activation',

'activation_email_resent' => 
'Email d\'activation renvoyé',

'can_access_site' => 
'Peut accéder au site',

'include_in_mailinglists' => 
'Autoriser les membres dans la liste de diffusion du site et aux capacités liées ? (par exemple les groupes disponibles dans la section Communiquer)',

'translate' => 
'Traduire',

''=>''
);

// End of File