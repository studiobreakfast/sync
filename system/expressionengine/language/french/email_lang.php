<?php

$lang = array(

//----------------------------
// Email
//----------------------------

"email_module_name" =>
"Email",

"email_module_description" =>
"Module d'email utilisateur",

'message_required' =>
"Le message de l'email est requis",

'em_banned_from_email' => 
"L'adresse email de l'expéditeur que vous avez saisie est exclue.",

'em_banned_recipient' =>
"Un ou plusieurs de vos destinataires emails sont exclus.",

'em_invalid_recipient' =>
"Un ou plusieurs de vos destinataires emails sont invalides.",

'em_no_valid_recipients' =>
"Votre email n'a pas de destinataires valide.",

'em_sender_required' =>
"Une adresse d'expéditeur valide est requise",

"em_unauthorized_request" =>
"Vous n'êtes pas autorisé à exécuter cette action",

'em_limit_exceeded' =>
"Vous avez dépassé le nombre d'emails autorisés à être envoyés par jour.",

'em_interval_warning' =>
"Vous ne pouvez soumettre les formulaires d'email que toutes les %s secondes",

"em_email_sent" =>
"Votre message email a été expédié.",



''=>''
);

/* End of file lang.email.php */
/* Location: ./system/expressionengine/language/french/lang.email.php */