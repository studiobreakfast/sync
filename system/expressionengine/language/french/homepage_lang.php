<?php

$lang = array(

//----------------------------
// Home page
//----------------------------

"install_lock_warning" =>
"Attention : Votre dossier d'installation est toujours sur le serveur.",

"install_lock_removal" =>
"Pour des raisons de sécurité, veuillez supprimer le dossier intitulé <strong>installer</strong> de votre serveur en utilisant votre logiciel FTP.",

"checksum_changed_warning" =>
"Un ou plusieurs fichiers principaux ont été endommagés :",

"checksum_changed_accept" =>
"Accepter les changements",

"checksum_email_subject" =>
"Un fichier principal a été modifié sur votre site.",

"checksum_email_message" =>
"ExpressionEngine a détecté la modification d'un fichier principal sur : {url}

Les fichiers suivants sont concernés :
{changed}

Si vous avez effectué ces changements, veuillez accepter les modifications sur l'accueil du tableau de bord. Si vous n'avez pas modifié ces fichiers ceci peut indiquer une tentative de piratage. Vérifiez les fichiers pour tout contenu suspect (JavaScript ou iFrames) et contactez le support ExpressionEngine :
http://expressionengine.com/forums/viewcategory/4/

Veuillez vérifier la politique du support avant de poster sur les forums :
http://expressionengine.com/support/policy/",

'new_version_notice' =>
'ExpressionEngine version %s est disponible.'."\n".'
<a href=\'%s\' title=\'Téléchargez ici\'>Téléchargez ici</a> et suivez les <a href=\'%s\' title=\'instructions de mise à jour\'>instructions de mise à jour</a>.',

'new_version_error' =>
'Une erreur s\'est produite en tentant de télécharger le numéro de version d\'ExpressionEngine. Veuillez vous rendre sur votre <a href=\'%s\' title=\'espace téléchargement\'>espace téléchargement</a> pour vérifier que vous utilisez la dernière version. Si l\'erreur persiste, veuillez prendre contact avec votre administrateur système.',

'important_messages' =>
'Information',


"cp_home" =>
"Ma page d'accueil",

"current_user" => 
"Utilisateur actuel:",

"system_status" =>
"Statut du système",

"offline" =>
"Arrêté",

"online" =>
"En marche",

"member_search" =>
"Recherche de membre",

"search_instructions" =>
"Saisissez des mot complets ou partiels",

"member_group" =>
"Groupe de membres",

"search_by" =>
"Rechercher dans le champ",

"screen_name" =>
"Nom à l'écran",

"email_address" =>
"Adresse email",

"url" =>
"URL",

"site_statistics" =>
"Statistiques du site",

"value" =>
"Valeur",

"total_members" =>
"Nombre de membres",

"total_validating_members" =>
"Membres en attente d'activation",

"total_validating_comments" =>
"Commentaires en attente de validation",

"total_entries" =>
"Nombre total d'articles",

"total_comments" =>
"Nombre total de commentaires",

"most_recent_entries" =>
"Articles les plus récents",

"most_recent_comments" =>
"Commentaires les plus récents",

"no_comments" =>
"Il n'y a actuellement aucun commentaire",

"no_entries" =>
"Il n'y a actuellement aucun article",

"entry_title" =>
"Titre de l'article",

"comments" =>
"Commentaires",

'no_channels_exist' =>
'Il n\'y a actuellement aucun canal',

'no_templates_available' =>
'Aucun modèle disponible',

"select_channel_to_post_in" => 
"Choisissez un canal dans lequel publier",

"recent_members" =>
"Nouveaux membres les plus récents",

"join_date" =>
"Date d'inscription",

"total_hits" =>
"Nombre de visites combinés des pages",

"demo_expiration" =>
"Votre compte démo expirera dans :",

'bulletin_board' =>
"Tableau des bulletins",

'no_bulletins' =>
"Aucun bulletin",

'bulletin_sender' =>
"Expéditeur du bulletin",

'bulletin_date' =>
"Date du bulletin",

'exact_match' =>
"Correspondance exacte",

'pmachine_news_feed' =>
'Flux d\'actualité d\'EllisLab',

'no_news' =>
'Aucune actualité disponible',

'more_news' =>
'Plus d\'actualités...',

"site_status" =>
"Statut du site",


''=>''
);

/* End of file lang.homepage.php */
/* Location: ./system/expressionengine/language/french/lang.homepage.php */