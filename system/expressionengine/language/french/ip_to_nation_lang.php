<?php

$lang = array(

//----------------------------------------
// Required for MODULES page
//----------------------------------------

"ip_to_nation_module_name" =>
"Localisation IP",

"ip_to_nation_module_description" =>
"Utilitaire associant les adresses IP et leur pays",

//----------------------------------------

"iptonation_missing" =>
"Impossible de trouver le fichier iptonation.php. Veuillez vous assurer que vous avez bien téléchargé tous les composants du module.",

"countryfile_missing" =>
"Impossible de trouver le fichier country.php dans votre dossier de configuration.",

"ip_search" =>
"Recherche d'adresse IP",

"ip_search_inst" =>
"Saisissez une adresse IP afin de déterminer à quel pays elle est associée",

"ip_result" =>
"L'adresse IP que vous avez saisie provient du pays suivant :",

"manage_banlist" =>
"Gestion de votre liste de pays exclus",

"country" =>
"Pays",

"ban_info" =>
"Sélectionnez les pays que vous désirez bannir. Lorsqu'un pays est banni, le titulaire de l'adresse IP correspondante n'est pas autorisé à soumettre commentaires, trackbacks ou à utiliser les formulaires Email et Recommander. Ils resteront toutefois capables de visualiser le site.",

"ban" =>
"Exclure",

"banlist" =>
"Liste des pays exclus",

"banlist_updated" =>
"La liste des exclus a été mise à jour avec succès",


''=>''
);

/* End of file lang.ip_to_nation.php */
/* Location: ./system/expressionengine/language/french/lang.ip_to_nation.php */