<?php

$lang = array(

/* ----------------------------------------
/*  Required for MODULES page
/* ----------------------------------------*/
"blogger_api_module_name" =>
"API Blogger",

"blogger_api_module_description" =>
"Module API Blogger",

/*----------------------------------------*/
'blogger_api_home' =>
"Tableau de bord",

'api_urls' =>
"Voir les URLs pour l'API Blogger",

'invalid_access' =>
"Accès invalide à l'API BLogger. Veuillez vérifier que vous avez utilisé les bons nom d'utilisateur et mot de passe pour un compte avec les permissions de publier dans au moins un canal.",

'no_channels_found' =>
"Aucun canal trouvé pour cet utilisateur.",

'invalid_channel' =>
"Sélection de canal invalide",

'invalid_categories' =>
"Sélection de catégories invalide",

'no_entries_found' =>
"Aucun article trouvé",

'no_entry_found' =>
"Article introuvable dans la base de données",

'unauthorized_action' =>
"Vous n'êtes pas autorisé à exécuter cette action.",

'invalid_access' =>
"Accès invalide",

'entry_uneditable' =>
"Vous n'avez pas la permission d'éditer cet article. Veuillez vérifier les permissions du groupe de membre.",

'channeler_configurations' =>
"Configurations de l'API Blogger",

'blogger_config_name' =>
"Nom",

'blogger_config_url' =>
"URL",

'no_blogger_configs' =>
"Il n'y a actuellement aucune configuration de l'API Blogger",

'blogger_delete_confirm' =>
"Supprimers les configurations de l'API Blogger",

'blogger_deleted' =>
"Configuration de l'API Blogger supprimée",

'bloggers_deleted' =>
"Configurations de l'API Blogger supprimées",

"blogger_delete_question" =>
"Etes-vous sûr de vouloir supprimer le(s) configuration(s)s de l'API Blogger sélectionnée(s) ?",

'delete' =>
"Supprimer",

'new_config' =>
"Nouvelle configuration",

'modify_config' =>
"Modifier la configuration",

'configuration_options' =>
"Options de configuration",

'blogger_pref_name' =>
"Nom de la configuration",

'blogger_default_field' =>
"Champ par défaut pour les données de l'article",

'blogger_default_field_subtext' =>
"Si aucun champ n'est spécifié pour une nouvel article ou si un article édité est envoyé comme un bloc de texte sans préciser un champ spécifique, ce champ sera celui où le contenu sera inséré. Assurez-vous que cet article soit disponible pour le canal où vous souhaitez poster.",

'blogger_html_format' =>
"Formatage HTML",

'blogger_html_format_subtext' =>
"Lorsqu'un programme compatible avec l'API Blogger demande un article, souhaitez-vous que le contenu soit envoyé avec les restrictions de formatage HTML ?",

'blogger_text_format' =>
"Envoyer un texte préformaté au programme client ?",

'blogger_text_format_subtext' =>
"Lors de l'édition d'article avec le programme client, souhaitez-vous que le texte soit formaté selon vos préférences ? Ce paramètre est normalement configuré sur 'non'",

'blogger_parse_type' => 
"Préférence de formatage de texte",

'blogger_parse_type_subtext' =>
"Requis pour les deux options suivantes ! Ceci affichera le contenu de l'article comme s'il s'agissait d'une page web avec tout le rendu BBCode et l'arborescence de fichier",

'html_format_none' =>
"Autoriser TOUT LE HTML",

'html_format_safe' =>
"Autoriser uniquement le HTML sûr",

'html_format_all' =>
"Convertir HTML en entité de caractères",

'blogger_block_entry' =>
"Envoyer les articles comme ",

'blogger_block_entry_subtext' =>
"L'API va combiner tous les champs personnalisés dans une chaîne de texte sans spécifier les champs avant de l'envoyer au programme client.",

'yes' =>
"Oui",

'no' =>
"Non",

'blogger_mising_fields' =>
"Un champ a été laissé vide",

'configuration_created' =>
"Configuration créée",

'configuration_updated' =>
"Configuration mise à jour",

'blogger_create_new' =>
"Créer une nouvelle configuration",



''=>''
);

/* End of file lang.blogger_api.php */
/* Location: ./system/expressionengine/language/french/lang.blogger_api.php */