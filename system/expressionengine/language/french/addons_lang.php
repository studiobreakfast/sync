<?php
$lang = array(


'addons' => 
'Add-ons',

'accessories' => 
'Accessoires',

'modules' => 
'Modules',

'extensions' => 
'Extensions',

'plugins' => 
'Plugins',

'accessory' => 
'Accessoire',

'module' => 
'Module',

'extension' => 
'Extension',

'addons_accessories' => 
'Accessoires',

'addons_modules' => 
'Modules',

'addons_plugins' => 
'Plugins',

'addons_extensions' => 
'Extensions',

'addons_fieldtypes' => 
'Types de champs',

'accessory_name' => 
'Nom de l\'accessoire',

'fieldtype_name' => 
'Nom du type de champ',

'install' => 
'Installer',

'uninstall' => 
'Désinstaller',

'installed' => 
'Installé',

'not_installed' => 
'Non installé',

'uninstalled' => 
'Désinstallé',

'remove' => 
'Supprimer',

'preferences_updated' => 
'Préférences mises à jour',

'extension_enabled' => 
'Extension activée',

'extension_disabled' => 
'Extension désactivée',

'extensions_enabled' => 
'Extensions activées',

'extensions_disabled' => 
'Extensions désactivées',

'delete_fieldtype_confirm' => 
'Etes-vous certain de vouloir supprimer ce type de champs ?',

'delete_fieldtype' => 
'Supprimer le type de champs',

'data_will_be_lost' => 
'Toutes les données associées à ce type de champs, y compris les données dans les canaux, seront effacées de façon irréversible',

'global_settings_saved' => 
'Paramètres enregistrés',

'package_settings' => 
'Paramètres du paquet',

'component' => 
'Composant',

'current_status' => 
'Statut courant',

'required_by' => 
'Utilisé par :',

'available_to_member_groups' => 
'Disponible aux groupes de membre',

'specific_page' => 
'Page spécifique ?',

'description' => 
'Description',

'version' => 
'Version',

'status' => 
'Statut',

'fieldtype' => 
'Type de champs',

'edit_accessory_preferences' => 
'Editer les préférences d\'accessoire',

'member_group_assignment' => 
'Groupes de membre assignés',

'page_assignment' => 
'Pages assignées',

'none' => 
'Aucun',

'and_more' => 
'et %x de plus...',

'plugins_not_available' => 
'Le plugin "Feed" est désactivé en version beta',

'no_extension_id' => 
'Aucune extension choisie',

'translate' => 
'Update',

''=>''
);

// End of File