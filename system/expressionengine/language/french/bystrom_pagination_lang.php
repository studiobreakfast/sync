<?php
$lang = array(
	'first_marker' => 'first_marker',
	'last_marker' => 'last_marker',
	'next_link' => 'last_marker',
	'prev_link' => 'prev_link',
	'text_first' => 'text_first',
	'text_last' => 'text_last',	
	'max_links' => 'Nombre de liens "numériques" à montrer avant/après la page courante',
	'first_div_o' => 'first_div_o',
	'first_div_c' => 'first_div_c',
	'next_div_o' => 'next_div_o',
	'next_div_c' => 'next_div_c',
	'prev_div_o' => 'prev_div_o',
	'prev_div_c'=> 'prev_div_c',
	'num_div_o' => 'num_div_o',
	'num_div_c' => 'num_div_c',
	'cur_div_o' => 'cur_div_o',
	'cur_div_c' => 'cur_div_c',
	'last_div_o' => 'last_div_o',
	'last_div_c' => 'last_div_c',
	'get_last_index' => 'Vous souhaiterez peut-être modifier la dernière étiquette pour ajouter une classe/ID afin d\'optimiser votre design (optionnel)',
	'set_last_index' => 'Remplacer la dernière étiquette avec ceci (optionnel)',
	'pagination_links_open' => 'pagination_links_open (optionnel)',
	'pagination_links_close' => 'pagination_links_close (optionnel)',
	'pre_start_tag' => 'Etiquette de pré-formatage (début) pour des options supplémentaires de mise en page (optionnel)',
	'pre_end_tag' => 'Etiquette de pré-formatage (fin) pour des options supplémentaires de mise en page (optionnel)',

	
	// END
	''=>''
	);
?>